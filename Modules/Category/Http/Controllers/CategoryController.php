<?php

namespace Modules\Category\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Category\Services\CategoryService;
use Modules\Category\Validators\CategoryValidator;

class CategoryController extends Controller
{

    /** @var  CategoryService */
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $response = $this->categoryService->paginate(10);

        return view('category::index', compact('response'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $request->validate(CategoryValidator::create($request));

        $this->categoryService->create($input);

        return redirect(route('admin.category.index'))->with("sucess", 'Categoria salvo com sucesso.');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $category = $this->categoryService->find($id);

        if (empty($category)) {
            return redirect(route('admin.category.index'))->with("error", "Categoria não encontrada");
        }

        return view('category::show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->categoryService->find($id);

        if (empty($category)) {
            return redirect(route('admin.category.index'))->with("error", "Categoria não encontrada");
        }

        return view('category::edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $category = $this->categoryService->find($id);

        if (empty($category)) {
            return redirect(route('admin.category.index'))->with("error", "Categoria não encontrada");
        }

        $request->validate(CategoryValidator::create($id, $request));
        $this->categoryService->update($request->all(), $id);

        return redirect(route('admin.category.index'))->with("sucess", 'Categoria salvo com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $category = $this->categoryService->find($id);

        if (empty($category)) {
            return redirect(route('admin.category.index'))->with("error", "Categoria não encontrada");
        }

        $this->categoryService->destroy($id);

        return redirect(route('admin.category.index'))->with("sucess", 'Post apagado com sucesso.');
    }
}
