<?php

namespace Modules\Category\Validators;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Prettus\Validator\LaravelValidator;

class CategoryValidator extends LaravelValidator
{
    /**
     * Get validation rules to create action.
     *
     * @param Request $request
     * @return array
     */
    public static function create($request)
    {
        return [
            'title' => [
                'required', 'max:50', Rule::unique('category_categories'),
            ]
        ];
    }

    /**
     * Get validation rules to update action.
     *
     * @param integer $id
     * @param Request $request
     * @return array
     */
    public static function update($id, $request)
    {
        return [
            'title' => [
                'required', 'max:50', Rule::unique('category_categories')->ignore($id, 'id'),
            ]
        ];
    }
}
