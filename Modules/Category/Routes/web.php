<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'],  function()
{
    Route::group(['prefix' => 'category'],  function() {
        Route::get('/', ['as' => 'admin.category.index', 'uses' =>  'CategoryController@index']);
        Route::get('create', ['as' => 'admin.category.create', 'uses' =>  'CategoryController@create']);
        Route::post('store', ['as' => 'admin.category.store', 'uses' =>  'CategoryController@store']);
        Route::get('show/{id}', ['as' => 'admin.category.show', 'uses' =>  'CategoryController@show']);
        Route::get('edit/{id}', ['as' => 'admin.category.edit', 'uses' =>  'CategoryController@edit']);
        Route::patch('update/{id}', ['as' => 'admin.category.update', 'uses' =>  'CategoryController@update']);
        Route::get('destroy/{id}', ['as' => 'admin.category.destroy', 'uses' =>  'CategoryController@destroy']);
    });

});