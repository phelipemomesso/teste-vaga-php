<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class CategoryDatabaseSeeder extends Seeder
{
    /**
     * The tables to be truncated before of seeding.
     *
     * @var array
     */
    protected $truncates = [
        'category_categories',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach ($this->truncates as $table) {

            Schema::disableForeignKeyConstraints();

            $this->command->getOutput()->writeln(sprintf('<comment>Truncating:</comment> %s', $table));
            DB::statement(sprintf('TRUNCATE TABLE %s;', $table));
            $this->command->getOutput()->writeln(sprintf('<info>Truncated:</info> %s', $table));

            Schema::enableForeignKeyConstraints();
        }

        $this->call(CategoriesTableSeeder::class);

        Model::reguard();
    }
}
