<?php

namespace Modules\Category\Services;

use Modules\Category\Entities\Category;
use Modules\Category\Repositories\CategoryRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoryService
{
    /**
     * The category repository instance.
     *
     * @var CategoryRepository
     */
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a paginated listing of the entity.
     *
     * @return LengthAwarePaginator
     */
    public function paginate($limit = false)
    {
        return $this->categoryRepository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'desc');
        })->paginate($limit);
    }

    /**
     * Find the specified entity.
     *
     * @param string $id
     * @return Category
     */
    public function find($id)
    {
        return $this->categoryRepository->find($id);
    }

    /**
     * Store a newly created entity in storage.
     *
     * @param array $attributes
     * @return Category
     */
    public function create(array $attributes)
    {
        $entity = $this->categoryRepository->skipPresenter(true)->create($attributes);
        return $this->categoryRepository->skipPresenter(false)->find($entity->id);
    }

    /**
     * Update the specified entity in storage.
     *
     * @param array $attributes
     * @param string $id
     * @return Category
     */
    public function update(array $attributes, $id)
    {
        $entity = $this->categoryRepository->skipPresenter(true)->find($id);
        $entity->update($attributes);
        return $this->categoryRepository->skipPresenter(false)->find($entity->id);
    }

    /**
     * Remove the specified entity from storage.
     *
     * @param  string $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->categoryRepository->skipPresenter(true)->find($id)->delete();
    }

    /**
     * Get the lobby repository instance.
     *
     * @return CategoryRepository
     */
    public function getRepository()
    {
        return $this->categoryRepository;
    }
}