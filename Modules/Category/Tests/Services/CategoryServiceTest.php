<?php

namespace Modules\Category\Tests\Services;

use Faker\Provider;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Artisan;
use Modules\Category\Entities\Category;
use Modules\Category\Services\CategoryService;
use Tests\TestCase;

class CategoryServiceTest extends TestCase
{
    use WithFaker;

    /**
     * The driver service instance.
     *
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');

        $this->faker->addProvider(new Provider\pt_BR\Person($this->faker));

        $this->categoryService = $this->app->make(CategoryService::class);
    }

    /**
     * Test it can store a newly created entity in storage.
     *
     * @return void
     */
    public function testItCanCreateEntity()
    {
        $values = [
            'title' => $this->faker->sentence,
            'status' => $this->faker->boolean,
            'description' => $this->faker->realText()
        ];

        $entity = $this->categoryService->create($values);
        $data = $entity->toArray();

        $this->assertDatabaseHas('category_categories', $values);
        $this->assertInstanceOf(Category::class, $entity);

        foreach ($this->dataStructure() as $key) {
            $this->assertArrayHasKey($key, $data);
        }
    }

    /**
     * Test it can display a listing of the entity.
     *
     * @return void
     */
    public function testItCanListingEntity()
    {
        $amount = 2;
        factory(Category::class, $amount)->create();

        $list = $this->categoryService->paginate();
        $data = current($list->items())->toArray();

        $this->assertInstanceOf(LengthAwarePaginator::class, $list);
        $this->assertEquals($amount, $list->total());

        foreach ($this->dataStructure() as $key) {
            $this->assertArrayHasKey($key, $data);
        }
    }

    /**
     * Test it can show the specified entity.
     *
     * @return void
     */
    public function testItCanShowEntity()
    {
        $fake = factory(Category::class)->create();
        $entity = $this->categoryService->find($fake->id);
        $data = $entity->toArray();

        $this->assertInstanceOf(Category::class, $entity);

        foreach ($this->dataStructure() as $key) {
            $this->assertArrayHasKey($key, $data);
        }
    }

    /**
     * Test it can update the specified entity in storage.
     *
     * @return void
     */
    public function testItCanUpdateEntity()
    {
        $fake = factory(Category::class)->create();
        $entity = $this->categoryService->find($fake->id);

        $data = [
            'title' => $this->faker->sentence,
            'status' => $this->faker->boolean,
            'description' => $this->faker->realText()
        ];

        $entity->update($data);

        $this->assertDatabaseHas('category_categories', $data);
    }

    /**
     * Test it can remove the specified entity from storage.
     *
     * @return void
     */
    public function testItCanDestroyEntity()
    {
        $entity = factory(Category::class)->create();

        $response = $this->categoryService->destroy($entity->id);

        $this->assertTrue($response);
    }

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    public function tearDown()
    {
        Artisan::call('migrate:reset');
        parent::tearDown();
    }

    /**
     * Structure of response entity.
     *
     * @return array
     */
    private function dataStructure()
    {
        return [
            'id',
            'title',
            'status',
            'description'
        ];
    }
}