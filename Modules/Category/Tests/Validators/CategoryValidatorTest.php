<?php

namespace Modules\Category\Tests\Validators;

use Illuminate\Validation\Rule;
use Modules\Category\Validators\CategoryValidator;
use Tests\TestCase;

class CategoryValidatorTest extends TestCase
{
    /**
     * The dock validator instance.
     *
     * @var CategoryValidator
     */
    protected $validator;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->validator = $this->app->make(CategoryValidator::class);
    }

    /**
     * Test it can create story validators.
     *
     * @return void
     */
    public function testItCanCreateStoryValidators()
    {
        $request = $this->getMockBuilder('Illuminate\Http\Request');

        $validators = [
            'title' => [
                'required', 'max:50', Rule::unique('category_categories'),
            ],
        ];

        $this->assertEquals($validators, CategoryValidator::create($request));
    }

    /**
     * Test it can create update validators.
     *
     * @return void
     */
    public function testItCanCreateUpdateValidators()
    {
        $request = $this->getMockBuilder('Illuminate\Http\Request');
        $id = 1;

        $validators = [
            'title' => [
                'required', 'max:50', Rule::unique('category_categories')->ignore($id, 'id'),
            ],
        ];

        $this->assertEquals($validators, CategoryValidator::update($id, $request));
    }

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }
}