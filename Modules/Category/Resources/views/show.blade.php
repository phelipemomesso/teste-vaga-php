@extends('layouts.app')

@section('content')
    <h2 class="page-header float-left">Categorias - Visualizar</h2>

    <br /><br />

    <div class="card">

        <div class="card-body">

            @include('category::show_fields')

            <div class="clearfix"></div>

            <a href="{!! route('admin.category.index') !!}" class="btn btn-primary">
                <i class="fas fa-arrow-left"></i>  Voltar
            </a>

        </div>
    </div>

@endsection
