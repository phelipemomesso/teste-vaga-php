@extends('layouts.app')

@section('content')
    <h2 class="page-header float-left">Categorias - Nova Categoria</h2>

    <br /><br />

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">

        <div class="card-body">

            {!! Form::open(['route' => 'admin.category.store']) !!}

            <div class="form-group">
                {!! Form::label('title', 'Título') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('status', 'Status') !!}
                {!! Form::select('status', [1=>'Ativo', 0 => 'Inativo'], null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descrição') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>

            <div class="clearfix"></div>

            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::button( '<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-success float-right', 'type'=>'submit']) !!}
                <a href="{!! route('admin.category.index') !!}" class="btn btn-primary"><i class="fa fa-times"></i> Cancelar </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

@endsection
