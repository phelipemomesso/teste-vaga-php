@extends('layouts.app')

@section('content')
    <h2 class="page-header float-left">Categorias</h2>

    <a class="btn btn-success float-right btn-tooltip" data-toggle="tooltip" data-original-title="Nova" href="{{ route('admin.category.create') }}">
        <i class="fas fa-plus-circle"></i> Nova Categoria
    </a>

    <br /><br />

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <table class="table table-striped">
        <thead>
            <tr>
                <td>Nome</td>
                <td>Status</td>
                <td>Ações</td>
            </tr>
        </thead>

        <tbody>
            @foreach($response as $category)
            <tr>
                <td>{{ $category->title }}</td>
                <td>{{ $category->status ? 'Ativo' : 'Inativo' }}</td>
                <td class="center">
                    <a class="btn btn-sm btn-info btn-tooltip" data-toggle="tooltip" data-original-title="Editar" href="{{ route('admin.category.edit', $category->id) }}">
                        <i class="fas fa-edit"></i>
                    </a>

                    <a class="btn btn-sm btn-info btn-tooltip" data-toggle="tooltip" data-original-title="Visualizar" href="{{ route('admin.category.show', $category->id) }}">
                        <i class="far fa-eye"></i>
                    </a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>

    <div>
        {{  $response->links() }}
    </div>

@stop
