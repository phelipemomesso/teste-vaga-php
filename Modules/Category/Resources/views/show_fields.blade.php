<form>
    <div class="form-row">

        <div class="form-group col-md-2">
            {!! Form::label('id', 'Id:') !!}
            <p>{!! $category->id !!}</p>
        </div>

        <div class="form-group col-md-10">
            {!! Form::label('title', 'Título:') !!}
            <p>{!! $category->title !!}</p>
        </div>

        <!-- Monthly Fee Field -->
        <div class="form-group col-md-4">
            {!! Form::label('status', 'Status:') !!}
            <p>{!! $category->status ? 'Ativo' : 'Inativo' !!}</p>
        </div>

        <!-- Created At Field -->
        <div class="form-group col-md-4">
            {!! Form::label('created_at', 'Data Cadastro:') !!}
            <p>{!! $category->created_at->format('d/m/Y H:i:s') !!}</p>
        </div>

        <div class="form-group col-md-4">
            {!! Form::label('updated_at', 'Data Update:') !!}
            <p>{!! $category->updated_at->format('d/m/Y H:i:s') !!}</p>
        </div>

        <!-- Registration Fee Field -->
        <div class="form-group col-md-12">
            {!! Form::label('description', 'Descrição:') !!}
            <p>{!! $category->description !!}</p>
        </div>

    </div>
</form>

