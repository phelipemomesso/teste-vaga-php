<?php

namespace Modules\Post\Validators;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Prettus\Validator\LaravelValidator;

class PostValidator extends LaravelValidator
{
    /**
     * Get validation rules to create action.
     *
     * @param Request $request
     * @return array
     */
    public static function create($request)
    {
        return [
            'title' => [
                'required', 'max:50', Rule::unique('post_posts'),
            ],
            'description' => [
                'required',
            ]
        ];
    }

    /**
     * Get validation rules to update action.
     *
     * @param integer $id
     * @param Request $request
     * @return array
     */
    public static function update($id, $request)
    {
        return [
            'title' => [
                'required', 'max:50', Rule::unique('post_posts')->ignore($id, 'id'),
            ],
            'description' => [
                'required',
            ]
        ];
    }
}
