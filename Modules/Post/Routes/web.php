<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'],  function()
{
    Route::group(['prefix' => 'post'],  function() {
        Route::get('/', ['as' => 'admin.post.index', 'uses' =>  'PostController@index']);
        Route::get('create', ['as' => 'admin.post.create', 'uses' =>  'PostController@create']);
        Route::post('store', ['as' => 'admin.post.store', 'uses' =>  'PostController@store']);
        Route::get('show/{id}', ['as' => 'admin.post.show', 'uses' =>  'PostController@show']);
        Route::get('edit/{id}', ['as' => 'admin.post.edit', 'uses' =>  'PostController@edit']);
        Route::patch('update/{id}', ['as' => 'admin.post.update', 'uses' =>  'PostController@update']);
        Route::get('destroy/{id}', ['as' => 'admin.post.destroy', 'uses' =>  'PostController@destroy']);
    });

});
