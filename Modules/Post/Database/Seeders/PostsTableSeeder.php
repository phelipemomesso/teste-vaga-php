<?php

namespace Modules\Post\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Category\Entities\Category;
use Modules\Post\Entities\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = Category::all();

        factory(Post::class, 30)->create()->each(function (Post $post) use ($categories) {
            $post->categories()->sync(
                $categories
                    ->random(mt_rand(1, 10))
                    ->pluck('id')
                    ->toArray()
            );
        });;
    }
}
