<?php

namespace Modules\Post\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class PostDatabaseSeeder extends Seeder
{
    /**
     * The tables to be truncated before of seeding.
     *
     * @var array
     */
    protected $truncates = [
        'post_posts',
        'category_post'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        Model::unguard();

        foreach ($this->truncates as $table) {

            Schema::disableForeignKeyConstraints();

            $this->command->getOutput()->writeln(sprintf('<comment>Truncating:</comment> %s', $table));
            DB::statement(sprintf('TRUNCATE TABLE %s;', $table));
            $this->command->getOutput()->writeln(sprintf('<info>Truncated:</info> %s', $table));

            Schema::enableForeignKeyConstraints();
        }

        $this->call(PostsTableSeeder::class);

        Model::reguard();


    }
}