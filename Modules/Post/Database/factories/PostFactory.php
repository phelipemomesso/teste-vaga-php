<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your module. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Modules\Post\Entities\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->sentence,
        'status' => $faker->boolean,
        'description' => $faker->realText(),
    ];
});