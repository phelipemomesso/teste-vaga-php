<?php

namespace Modules\Post\Repositories;

use Modules\Post\Entities\Post;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

class PostRepositoryEloquent extends BaseRepository implements PostRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title' => 'ilike',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Post::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
