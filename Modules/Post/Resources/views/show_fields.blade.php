<form>
    <div class="form-row">

        <div class="form-group col-md-2">
            {!! Form::label('id', 'Id:') !!}
            <p>{!! $post->id !!}</p>
        </div>

        <div class="form-group col-md-10">
            {!! Form::label('title', 'Título:') !!}
            <p>{!! $post->title !!}</p>
        </div>

        <!-- Monthly Fee Field -->
        <div class="form-group col-md-4">
            {!! Form::label('status', 'Status:') !!}
            <p>{!! $post->status ? 'Ativo' : 'Inativo' !!}</p>
        </div>

        <!-- Created At Field -->
        <div class="form-group col-md-4">
            {!! Form::label('created_at', 'Data Cadastro:') !!}
            <p>{!! $post->created_at->format('d/m/Y H:i:s') !!}</p>
        </div>

        <div class="form-group col-md-4">
            {!! Form::label('updated_at', 'Data Update:') !!}
            <p>{!! $post->updated_at->format('d/m/Y H:i:s') !!}</p>
        </div>

        <!-- Registration Fee Field -->
        <div class="form-group col-md-12">
            {!! Form::label('description', 'Descrição:') !!}
            <p>{!! $post->description !!}</p>
        </div>

    </div>
</form>

