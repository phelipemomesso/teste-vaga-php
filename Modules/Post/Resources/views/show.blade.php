@extends('layouts.app')

@section('content')
    <h2 class="page-header float-left">Post - Visualizar</h2>

    <br /><br />

    <div class="card">

        <div class="card-body">

            @include('post::show_fields')

            <div class="clearfix"></div>

            <a href="{!! route('admin.post.index') !!}" class="btn btn-primary">
                <i class="fas fa-arrow-left"></i>  Voltar
            </a>

        </div>
    </div>

@endsection
