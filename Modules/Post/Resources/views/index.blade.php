@extends('layouts.app')

@section('content')
    <h2 class="page-header float-left">Posts</h2>

    <a class="btn btn-success float-right btn-tooltip" data-toggle="tooltip" data-original-title="Nova" href="{{ route('admin.post.create') }}">
        <i class="fas fa-plus-circle"></i> Novo Post
    </a>

    <br /><br />

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <table class="table table-striped">
        <thead>
            <tr>
                <td>Nome</td>
                <td>Status</td>
                <td>Ações</td>
            </tr>
        </thead>

        <tbody>
            @foreach($response as $post)
            <tr>
                <td>{{ $post->title }}</td>
                <td>{{ $post->status ? 'Ativo' : 'Inativo' }}</td>
                <td class="center">
                    <a class="btn btn-sm btn-info btn-tooltip" data-toggle="tooltip" data-original-title="Editar" href="{{ route('admin.post.edit', $post->id) }}">
                        <i class="fas fa-edit"></i>
                    </a>

                    <a class="btn btn-sm btn-info btn-tooltip" data-toggle="tooltip" data-original-title="Visualizar" href="{{ route('admin.post.show', $post->id) }}">
                        <i class="far fa-eye"></i>
                    </a>

                    <a class="btn btn-sm btn-danger btn-tooltip" data-toggle="tooltip" data-original-title="Excluir" href="{{ route('admin.post.destroy', $post->id) }}">
                        <i class="far fa-trash-alt"></i>
                    </a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>

    <div>
        {{  $response->links() }}
    </div>

@stop
