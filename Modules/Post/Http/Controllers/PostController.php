<?php

namespace Modules\Post\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Post\Services\PostService;
use Modules\Post\Validators\PostValidator;

class PostController extends Controller
{
    /** @var  PostService */
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $response = $this->postService->paginate(10);

        return view('post::index', compact('response'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('post::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $request->validate(PostValidator::create($request));

        $this->postService->create($input);

        return redirect(route('admin.post.index'))->with('success', 'Post salvo com sucesso.');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $post = $this->postService->find($id);

        if (empty($post)) {
            return redirect(route('admin.post.index'))->with('error', 'Post não encontrado.');
        }

        return view('post::show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $post = $this->postService->find($id);

        if (empty($post)) {
            return redirect(route('admin.post.index'))->with('error', 'Post não encontrado.');
        }

        return view('post::edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $post = $this->postService->find($id);

        if (empty($post)) {
            return redirect(route('admin.post.index'))->with('error', 'Post não encontrado.');
        }

        $request->validate(PostValidator::update($id, $request));
        $this->postService->update($request->all(), $id);

        return redirect(route('admin.post.index'))->with('success', 'Post salvo com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $post = $this->postService->find($id);

        if (empty($post)) {
            return redirect(route('admin.post.index'))->with('error', 'Post não encontrado.');
        }

        $this->postService->destroy($id);

        return redirect(route('admin.post.index'))->with('success', 'Post apagado com sucesso.');
    }
}
