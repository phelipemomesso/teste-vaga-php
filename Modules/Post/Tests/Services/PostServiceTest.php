<?php

namespace Modules\Post\Tests\Services;

use Faker\Provider;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Artisan;
use Modules\Post\Entities\Post;
use Modules\Post\Services\PostService;
use Tests\TestCase;

class PostServiceTest extends TestCase
{
    use WithFaker;

    /**
     * The driver service instance.
     *
     * @var PostService
     */
    protected $postService;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');

        $this->faker->addProvider(new Provider\pt_BR\Person($this->faker));

        $this->postService = $this->app->make(PostService::class);
    }

    /**
     * Test it can store a newly created entity in storage.
     *
     * @return void
     */
    public function testItCanCreateEntity()
    {
        $values = [
            'title' => $this->faker->sentence,
            'status' => $this->faker->boolean,
            'description' => $this->faker->realText()
        ];

        $entity = $this->postService->create($values);
        $data = $entity->toArray();

        $this->assertDatabaseHas('post_posts', $values);
        $this->assertInstanceOf(Post::class, $entity);

        foreach ($this->dataStructure() as $key) {
            $this->assertArrayHasKey($key, $data);
        }
    }

    /**
     * Test it can display a listing of the entity.
     *
     * @return void
     */
    public function testItCanListingEntity()
    {
        $amount = 2;
        factory(Post::class, $amount)->create();

        $list = $this->postService->paginate();
        $data = current($list->items())->toArray();

        $this->assertInstanceOf(LengthAwarePaginator::class, $list);
        $this->assertEquals($amount, $list->total());

        foreach ($this->dataStructure() as $key) {
            $this->assertArrayHasKey($key, $data);
        }
    }

    /**
     * Test it can show the specified entity.
     *
     * @return void
     */
    public function testItCanShowEntity()
    {
        $fake = factory(Post::class)->create();
        $entity = $this->postService->find($fake->id);
        $data = $entity->toArray();

        $this->assertInstanceOf(Post::class, $entity);

        foreach ($this->dataStructure() as $key) {
            $this->assertArrayHasKey($key, $data);
        }
    }

    /**
     * Test it can update the specified entity in storage.
     *
     * @return void
     */
    public function testItCanUpdateEntity()
    {
        $fake = factory(Post::class)->create();
        $entity = $this->postService->find($fake->id);

        $data = [
            'title' => $this->faker->sentence,
            'status' => $this->faker->boolean,
            'description' => $this->faker->realText()
        ];

        $entity->update($data);

        $this->assertDatabaseHas('post_posts', $data);
    }

    /**
     * Test it can remove the specified entity from storage.
     *
     * @return void
     */
    public function testItCanDestroyEntity()
    {
        $entity = factory(Post::class)->create();

        $response = $this->postService->destroy($entity->id);

        $this->assertTrue($response);
    }

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    public function tearDown()
    {
        Artisan::call('migrate:reset');
        parent::tearDown();
    }

    /**
     * Structure of response entity.
     *
     * @return array
     */
    private function dataStructure()
    {
        return [
            'id',
            'title',
            'status',
            'description'
        ];
    }
}