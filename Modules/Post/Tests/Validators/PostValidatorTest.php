<?php

namespace Modules\Post\Tests\Validators;

use Illuminate\Validation\Rule;
use Modules\Post\Validators\PostValidator;
use Tests\TestCase;

class PostValidatorTest extends TestCase
{
    /**
     * The dock validator instance.
     *
     * @var PostValidator
     */
    protected $validator;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->validator = $this->app->make(PostValidator::class);
    }

    /**
     * Test it can create story validators.
     *
     * @return void
     */
    public function testItCanCreateStoryValidators()
    {
        $request = $this->getMockBuilder('Illuminate\Http\Request');

        $validators = [
            'title' => [
                'required', 'max:50', Rule::unique('post_posts'),
            ],
            'description' => [
                'required',
            ]
        ];

        $this->assertEquals($validators, PostValidator::create($request));
    }

    /**
     * Test it can create update validators.
     *
     * @return void
     */
    public function testItCanCreateUpdateValidators()
    {
        $request = $this->getMockBuilder('Illuminate\Http\Request');
        $id = 1;

        $validators = [
            'title' => [
                'required', 'max:50', Rule::unique('post_posts')->ignore($id, 'id'),
            ],
            'description' => [
                'required',
            ]
        ];

        $this->assertEquals($validators, PostValidator::update($id, $request));
    }

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }
}