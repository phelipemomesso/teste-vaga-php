<?php

namespace Modules\Post\Services;

use Modules\Post\Entities\Post;
use Modules\Post\Repositories\PostRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class PostService
{
    /**
     * The post repository instance.
     *
     * @var PostRepository
     */
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Display a paginated listing of the entity.
     *
     * @return LengthAwarePaginator
     */
    public function paginate($limit = false)
    {
        return $this->postRepository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'desc');
        })->paginate($limit);
    }

    /**
     * Find the specified entity.
     *
     * @param string $id
     * @return Category
     */
    public function find($id)
    {
        return $this->postRepository->find($id);
    }

    /**
     * Store a newly created entity in storage.
     *
     * @param array $attributes
     * @return Category
     */
    public function create(array $attributes)
    {
        $entity = $this->postRepository->skipPresenter(true)->create($attributes);
        return $this->postRepository->skipPresenter(false)->find($entity->id);
    }

    /**
     * Update the specified entity in storage.
     *
     * @param array $attributes
     * @param string $id
     * @return Category
     */
    public function update(array $attributes, $id)
    {
        $entity = $this->postRepository->skipPresenter(true)->find($id);
        $entity->update($attributes);
        return $this->postRepository->skipPresenter(false)->find($entity->id);
    }

    /**
     * Remove the specified entity from storage.
     *
     * @param  string $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->postRepository->skipPresenter(true)->find($id)->delete();
    }

    /**
     * Get the post repository instance.
     *
     * @return PostRepository
     */
    public function getRepository()
    {
        return $this->postRepository;
    }
}